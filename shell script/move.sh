#!/bin/bash

# Menampilkan kapasitas file yang didownload
echo "Kapasitas file yang didownload: "
du -h "$filename"

# Memindahkan file ke dalam folder "folder_download" dengan tipe file yang sama
mv "$filename" "$foldername/${filename%.*}"_"$(date +%Y%m%d_%H%M%S)"."${filename##*.}"
