# 1. Resource Monitoring
### RAM
Agar dapat memonitor penggunaan memori RAM, dapat menggunakan command `free`. 
Contoh output yang ditampilakan:

```bash
praktikumc@praktikumsistemoperasi ~
 % free
               total        used        free      shared  buff/cache   available
Mem:         2030420      300088      128336         596     1601996     1550824
Swap:              0           0           0
```
Pada defaultnya, satuan yang akan ditampilkan dalam bentuk kilobyte. Untuk mengetahui satuan yang ditampilkannya dalam satuan megabyte, kita bisa menggunakan flag `--mega`. 
Contoh Output yang ditampilkan:

```bash
praktikumc@praktikumsistemoperasi ~
 % free --mega
               total        used        free      shared  buff/cache   available
Mem:            2079         317         120           0        1640        1577
Swap:              0           0           0

```
### CPU
Dalam melihat penggunaan CPU, kita dapat menggunakan command `top` atau `ps -aux`. 
Contoh Output yang ditampilkan:

- Dengan menggunakan `top`
![top](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/job-interview-so/-/raw/main/screenshoot/top.png)

- Dengan menggunakan `ps -aux`

![top](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/job-interview-so/-/raw/main/screenshoot/ps_-aux.png)

### Disk
Dalam memonitor penggunaan disk, dapat menggunakan command `lsblk` atau `df`. Untuk melihat penggunaan disk dengan tampilan yang dapat dibaca manusia, dapat menggunakan command `df -h`.
Contoh Output yang ditampilkan:

```bash
praktikumc@praktikumsistemoperasi ~
 % lsblk
NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
vda     254:0    0   40G  0 disk
├─vda1  254:1    0 39.9G  0 part /
├─vda14 254:14   0    3M  0 part
└─vda15 254:15   0  124M  0 part /boot/efi

```
```bash

praktikumc@praktikumsistemoperasi ~
 % df
Filesystem     1K-blocks    Used Available Use% Mounted on
udev              997040       0    997040   0% /dev
tmpfs             203044     608    202436   1% /run
/dev/vda1       41111748 3670944  35732328  10% /
tmpfs            1015208       0   1015208   0% /dev/shm
tmpfs               5120       0      5120   0% /run/lock
/dev/vda15        126678   10900    115778   9% /boot/efi
tmpfs             203040       0    203040   0% /run/user/1001

```
```bash
praktikumc@praktikumsistemoperasi ~
 % df -h
Filesystem      Size  Used Avail Use% Mounted on
udev            974M     0  974M   0% /dev
tmpfs           199M  576K  198M   1% /run
/dev/vda1        40G  3.8G   34G  10% /
tmpfs           992M     0  992M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/vda15      124M   11M  114M   9% /boot/efi
tmpfs           199M     0  199M   0% /run/user/1001
```

# 2. Program Management
## Memonitor program yang berjalan
Untuk melakukan monitoring program yang sedang berjalan, dapat menggunakan command seperti sebelumnyapada pengecekan CPU yaitu dengan perintah `top`, atau `ps`.

## Menghentikan program yang berjalan
Menghentikan program yang sedang berjalan, kita dapat menggunakan command `kill`, kemudian masukkan nomor PID-nya.

Untuk mengetahui PID dari program tersebut, dapat dilakukan dengan perintah berikut:

```bash
ps -ef | grep <program/file>.<format-file>
```

```bash
kill <PID>
```
## Otomasi perintah dengan shell script
Pertama cek terlebih dahulu shell yang sedang digunakan. Pengecekan dapat dilakukan dengan perintah `which bash` atau `echo $SHELL`. 

Contoh Output:

```bash
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra
 % which bash
/usr/bin/bash
```
```bash
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra
 % echo $SHELL
/usr/bin/zsh
```
Lalu membuat file script-nya dengan menggunakan perintah `touch`.

```bash
touch name-script.sh
```
Contoh Output:
```bash
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra
 % touch name-script.sh
```

Buka file .sh tersebut dengan menggunakan teks editor atau dengan perintah `vim`, pada baris pertama isikan `#! /bin/bash` untuk menentukan interpreter dalma mengakses script, dalam kasus ini adalah **Bash** SHELL.

```bash
% vim script-name.sh
```
```bash
#! bin/bash
```
Kemudian, masuk kedalam mode Insert dengan menekan keyboard **i** untuk membuat pengkodean perintah-perintah bash. Untuk kembali kedalam mode Visual dengan menekan keyboard **esc**. kemudian masukkan perintah `:wq` untuk keluar dari mode vim dan menyimpannya.

Untuk mengeksekusi script tersebut, terlebih dahulu harus mengganti permission dari file script tersebut agar dapat dieksekusi.

```bash
chmod +x script-name.sh
```
Untuk mengeksekusinya dapat dilakuakan dengan perintah. Apabila script berada di directory lain disesuaikan saja directorynya.

```bash
<directory> /script-name.sh
```
## Penjadwalan eksekusi program dengan cron
Melakukan eksekusi dengan `cron`, dapat dilakukan dengan melakukan perintah `crontab -e`, selanjutnya sistem akan membuka file crontab di teks editor default.

```bash
crontab -e
```
Cara penggunaan `crontab` dapat dilakukan seperti berikut:
```bash
* * * * * command
```
Dibagian command diisi dengan file script yang akan dijalankan. 

Contoh: Menjadwalkan script untuk berjalan setiap hari jam 8 pagi.

```bash
0 8 * * * home/praktikumc/1217050137-sumitra/penjadwalan.sh
```
# 3. Network Management
## Akses sistem operasi pada jaringan menggunakan SSH
Untuk mengakses sistem operasi pada jaringan dengan menggunakan SSH dapat dilakukan dengan membuka atau mengakses terminal di komputer, kemudian memasukkan perintah SSH dengan memasukkan nama domain dan alamat IP.

1. Akses hanya dengan IP

```bash
ssh <account>@<public-ip>
```
2. Dapat diakses juga dengan menggunakan port 

```bash
ssh <account>@<public-ip> -p <port>
```
Contohnya akan mengakses `ssh praktikumc@103.82.93.37 -p 22`.
![top](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/job-interview-so/-/raw/main/screenshoot/ssh.gif)

## Memonitor program yang menggunakan network
Untuk memonitor program yang menggunakan network dapat dilakukan dengan melakukan perintah `netstat` pada terminal. Perintah tersebut akan menampilkan statistika jaringan koneksi pada sistem operasi, termasuk port dan alamat IP yang digunakan, proses yang sedang terjadi dan status koneksi.

![top](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/job-interview-so/-/raw/main/screenshoot/network.png)

Pada perintah dapat menambahkan flag `-atnp` untuk mengetahui statistika koneksi, dimana:
- a : menampilkan semua koneksi
- t : menampilkan koneksi TCP
- n : menampilkan nomor port 
- p : menampilkan nama program yang terkait dengan koneksi tersebut

![top](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/job-interview-so/-/raw/main/screenshoot/netstat.png)

## Mampu mengoperasikan HTTP client
Untuk mengoperasikan HTTP client,dapat dilakukan dengan perintah `curl`. 

```bash
culr <Link-Url>
```
Selain itu dapat dengan perintah `wget` untuk mengunduh  file dari server yang menggunakan protokol HTTPS, HTTP, dan FTP.

```bash
wget <Link-Url>
```
Contoh mendownload foto doraemon dari website:

```bash
wget https://vignette.wikia.nocookie.net/th-doraemon/images/c/c0/Doraemon_(2002).png/revision/latest?cb=20170406070443
```
# 4. File and Folder Management
## Navigasi
Untuk melakukan navigasi pada direktori dapat melakukan commend berikut:
```bash
cd
pwd
ls 
```
- **cd** untuk berpindah direktori.
- **ls** menampilkan isi dari sebuah direktori yang sedang ditempati atau dibuka.
- **pwd** menampilkan lokasi direktori yang sedang ditempati atau dibuka.

contoh:

![top](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/job-interview-so/-/raw/main/screenshoot/navigasi.png)

## CRUD file dan Folder
- Penggunaan editor
CRUD dapat dilakukan dengan teks editor seperti Vim. Vim merupakan suatu teks editor yang sangat populer dilingkungan LINUX dan UNIX, serta Vim dapat menambahkan sejumlah fitur tambahan ke vi, dan mendukung fitur yang lebih banyak untuk pengeditan teks.

```bash
vim <name-file>.<format-file>
```
Contoh:
```bash
praktikumc@praktikumsistemoperasi ~
 % vim index.txt
```
Saat melakukan perintah vim, Otomatis akan membuka teks editor vim. Saat sudah masuk kedalam teks editor vim, teks editor masih dalam mode visual sehingga diperlukan beberapa perintah seperti:
1. Tekan tombol `i` pada keyboard untuk melakukan pengeditan atau melakukan pengetikan kode.
2. Tekan `esc` pada keyboard jika telash selesai melakukan pengkodean untuk kembali kedalam mode visual.
3. Tekan tombol `:w` pada keyboard untuk menyimpan hasil pengkodean kedalam isi file.
4. Tekan tombol `:q` pada keyboar untuk keluar dari mode visual vim.

- CRUD file dan folder
CRUD file dan folder dapat dilakukan dengan command:
Contoh:
```bash
mkdir
touch
rm 
mv
cp 
cat 
rmdir
rm -r
```
- **mkdir**, membuat direktori
- **touch**, membuat file
- **rm**, menghapus file atau direktori
- **mv**, cut atau move file atau direktori
- **cp**, copy file atau direktori
- **cat**, membaca isi file
- **rmdir**, menghapus direktori, jika direktorinya kosong
- **rm -r**, menghapus direktori, jika direktorinya tidak kosong

## Manajemen ownership dan akses file dan folder
Dalam managemen ownership akses file dan direktori pada Linux, dapat dilakukan dengan menggunakan command `chown` dan `chmod`.

```bash
chown [user:grup] <name-file.format-file>
```
- **user:grup**, nama owner/grup mana yang akan menjadi ownernya
- **name-file**, nama file yang akan diubah permissionnya

```bash
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra
 % sudo chown 1217050137-sumitra:praktikumc biodata.txt
[sudo] password for praktikumc:
praktikumc is not in the sudoers file.  This incident will be reported.
```
Penggunaan command `chmod` dapat dilakukan dengan format:
```bash
chmod +[access] <name-file.format-file>
```
- **access**, untuk mengubah akses izin
atau

```bash
chmod <***> <name-file.format-file>
```
- <***> diubah menjadi 3 digit untuk akses izin

```bash
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra/test
 % touch data.txt
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra/test
 % chmod +x data.txt
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra/test
 % chmod 750 data.txt
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra/test
 % ls -l
total 0
-rwxr-x--- 1 praktikumc praktikumc 0 May 11 18:11 data.txt
```

## Pencarian file dan folder
Untuk mencari file atau direktori dapat menggunakan perintah `find` dan `grep`. Penggunaan `find` dapat digunakan untuk mencari file atau direktori berdasarkan nama, tanggal modifikasi, dan ukuran. Sedangkan `grep` digunakan untuk mencari mencari pola atau teks tertentu dalam satu atau beberapa file teks.

- Pencarian dengan find
File

```bash
find /home/user/ -name "contoh.txt"
```
contohnya:

```bash
praktikumc@praktikumsistemoperasi ~
 % find /home/praktikumc/1217050137-sumitra -name "data.txt"
/home/praktikumc/1217050137-sumitra/data.txt
/home/praktikumc/1217050137-sumitra/test/data.txt
```
Find file berdasarkan tipe

```bash
find /lokasi/awal -type tipefile
```
Contohnya:

```bash
praktikumc@praktikumsistemoperasi ~
 % find /home/praktikumc/1217050137-sumitra -type f -name "*.txt"
/home/praktikumc/1217050137-sumitra/data.txt
/home/praktikumc/1217050137-sumitra/linux/belajarlinux.txt
/home/praktikumc/1217050137-sumitra/access.txt
/home/praktikumc/1217050137-sumitra/pertemuan-2/kampus/fakultas-a/jurusan-a1/mahasiswa/mahasiswa-m1/tugas/tugas-1.txt
/home/praktikumc/1217050137-sumitra/pertemuan-2/kampus/fakultas-a/jurusan-a1/mahasiswa/mahasiswa-m1/tugas/tugas-2.txt
/home/praktikumc/1217050137-sumitra/test/data.txt
/home/praktikumc/1217050137-sumitra/data-name.txt
```
Find direktori kosong
Pada dasarnya, perintah find untuk mencari direktori sama seperti untuk mencari file, yang membedakannya adalah pada tanda **f** yang menunjukkan file, diubah menjadi **d** yang artinya direktori.

```bash
praktikumc@praktikumsistemoperasi ~
 % find /home/praktikumc/1217050137-sumitra -type d -empty 
/home/praktikumc/1217050137-sumitra/pertemuan-2/kampus/fakultas-a/jurusan-a2
/home/praktikumc/1217050137-sumitra/pertemuan-2/kampus/fakultas-a/jurusan-a1/mahasiswa/mahasiswa-m2
/home/praktikumc/1217050137-sumitra/pertemuan-2/kampus/fakultas-a/jurusan-a1/mahasiswa/mahasiswa-m3
/home/praktikumc/1217050137-sumitra/pertemuan-2/kampus/fakultas-a/jurusan-a3
/home/praktikumc/1217050137-sumitra/pertemuan-2/kampus/fakultas-c
/home/praktikumc/1217050137-sumitra/pertemuan-2/kampus/fakultas-b
/home/praktikumc/1217050137-sumitra/dino-world/backend
/home/praktikumc/1217050137-sumitra/dino-world/frontend/src

```
- Pencarian dengan grep
```bash
grep <pattern> /home/user/contoh.txt
```
Misalkan kita akan menccari kata LINUX pada file.

```bash
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra
 % grep "LINUX" /home/praktikumc/1217050137-sumitra/biodata.txt
LINUX
```
## Kompresi data
Untuk kompresi data dapat menggunakan perintah `tar` . Contohnya kita akan mengkompresi seluruh file berformat `.txt` pada direktori

```bash
% tar -czvf file-name.tar.gz *.txt

```
- **tar**, kumpulan file tanpa kompresi
- **gz**, gzip atau file yang telah dikompresi
- **c**, create archieve
- **z**, gzip compress
- **v**, hasil detail dengan informasi tambahan
- **f**, name archieve

Output yang dihasilkan:

```bash
praktikumc@praktikumsistemoperasi ~/1217050137-sumitra
 % tar -czvf file-name.tar.gz *.txt
access.txt
biodata.txt
data-name.txt
data.txt

```
# 5. Shell scripting
Dalam hal ini, perintah otomasi dapat dibuat dengan shell script. 
- Tujuan shell script otomasi yang dibuat: untuk mendownload file dari website dengan menggunakan perintah `curl` dan `wget`memindahkannya kedalam folder. 
[Shell Script](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/job-interview-so/-/blob/main/shell%20script/file-download.sh)
# 6. Shell script Demonstration
Demontrasi dari otomasi shell script tersebut dapat diakses melalui [via youtube](https://youtu.be/j2vf2Qj56Io) 
# 7. Maze Game
Selamat Datang Di **Dunia Dino**. Tambah pengetahuanmu seputar dinosaurus disini. Disini kamu akan mengetahui fakta-fakta menarik terkait dinosaurus. Tantang pengetauanmu dengan berlatih permainan ini ya. [**Ayo Mainkan!**](https://maze.informatika.digital/maze/strd/)
# 8. Initiative in Project
## Tools
Inisiatif dalam kasus ini adalah pengaksesan SSH dapat dilakukan Visual Studio Code (VSCode) dalam mengakses suatu Network. Pengaksesan dapat dilakukan dengan menambahkan extention **Remote SS** pada VSCode. Tampilan dari penggunaan extention tersebut dapat dilihat seperti berikut.
![tampilan](screenshoot/tools.png) 
